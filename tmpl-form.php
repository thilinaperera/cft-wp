<?php
/*
Template Name: Custom Tour template
*/


if(isset($_POST['customize_tour'])){
    $data = $_POST['customize_tour'];
    $message = '<h3>Tour Request</h3>';
    $destination_we_support ='';
    $wishList = '';
    $name = '';
    foreach($data as $field){


        if($field['name'] == 'name'){
            $name = $field['value'];
            $message .= '<b>Name</b> : '.$field['value'].'</br>';
        }
        if($field['name'] == 'email'){
            $message .= '<b>Email</b> : '.$field['value'].'</br>';
        }
        if($field['name'] == 'checkin'){
            $message .= '<b>Check In</b> : '.$field['value'].'</br>';
        }
        if($field['name'] == 'checkout'){
            $message .= '<b>Check Out</b> : '.$field['value'].'</br>';
        }
        if($field['name'] == 'guests'){
            $message .= '<b>Guests</b> : '.$field['value'].'</br>';
        }
        if($field['name'] == 'levelOfAccommodation'){
            $message .= '<b>Level of accommodation</b> : '.$field['value'].'</br>';
        }
        if($field['name'] == 'destination_we_support'){
            $destination_we_support .= ''.$field['value'].', ';
        }
        if($field['name'] == 'wishList'){
            foreach($field['value'] as $value){
                $wishList .= ''.$value.', ';
            }
        }
        if($field['name'] == 'comments'){
            $message .= '<b>Comments</b> : '.$field['value'].'</br>';
        }

    }
    $message .= '<b>Destinations</b> : '.$destination_we_support.'</br>';
    $message .= '<b>Wish list</b> : '.$wishList.'</br>';
    //echo $message;

    $blogusers = get_users( array( 'fields' => array( 'user_email' ) ) );
    foreach ( $blogusers as $user ) {
        //echo json_encode($user->user_email);
        add_filter('wp_mail_content_type',create_function('', 'return "text/html"; '));
        wp_mail( $user->user_email, 'Tour Request', $message );
    }
    echo "{
        'status' : '200',
        'message':'Thank you ".$name.". We will contact you within 1-2 business days.'
    }";
    die;

}

$getData = array();
//echo json_encode($_GET);
if(isset($_GET['checkin'])){
    //echo $_GET['checkin'];
    $getData['checkin'] = $_GET['checkin'];
}
if(isset($_GET['checkout'])){
    $getData['checkout'] = $_GET['checkout'];
}
if(isset($_GET['guests'])){
    $getData['guests'] = $_GET['guests'];
}

get_header();
?>
    <div class="sectionTopImage">
        <div class="container">
            <?php

            $theFullThumb_URL =  wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ) );


            ?>
            <div class="row topImageRow" style="background: url('<?= $theFullThumb_URL ?>') center">

                <div class="imageText">
                    <div class="text container">
                        <div class="row textDetails">
                            <h3> <?= get_the_title(); ?> </h3>
                            <span class="subText"><?= get_post_meta(get_the_ID(), 'sub_heading', true);  ?></span>
                        </div>
                    </div>
                </div>
                <div class="flipper"></div>
            </div>
        </div>
    </div>

    <section class="postSection">
        <div class="container postWithSidebar">
            <div class="row postRow">


                <?php get_sidebar(); ?>

                <div class="col-lg-8 col-md-8 postText">

                    <form id="customizeTour" name="customizeTour" class="form-horizontal" style="padding: 1rem 3rem">
                        <h3>Please enter tour details</h3>

                        <div class="form-group">
                            <label for="name" class="col-sm-3 control-label">Name *</label>
                            <div class="col-sm-9">
                                <input name="name" type="text" class="form-control name" placeholder="Name" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="col-sm-3 control-label">Email *</label>
                            <div class="col-sm-9">
                                <input name="email" type="email" class="form-control email" placeholder="Email" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="checkin" class="col-sm-3 control-label">Check in</label>
                            <div class="col-sm-9">

                                <input value="<?= $getData['checkin'] ?>" name="checkin" type="text" class="form-control datepicker checkin" placeholder="Check in">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="checkout" class="col-sm-3 control-label">Check out</label>
                            <div class="col-sm-9">
                                <input value="<?= $getData['checkout'] ?>" name="checkout" type="text" class="form-control datepicker checkout" placeholder="Check out">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="guests" class="col-sm-3 control-label">How many guests</label>
                            <div class="col-sm-9">
                                <input value="<?= $getData['guests'] ?>" name="guests" type="text" class="form-control guests" id="" placeholder="How many guests">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="levelOfAccommodation" class="col-sm-3 control-label">Level of Accommodation</label>
                            <div class="col-sm-9">
                                <label class="radio-inline">
                                    <input type="radio" name="levelOfAccommodation"  value="5 stars" checked> 5 stars
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="levelOfAccommodation"  value="4 stars"> 4 stars
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="levelOfAccommodation"  value="3 stars"> 3 stars
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="levelOfAccommodation"  value="other"> other
                                </label>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-9">
                                <label>Please select destination(s)</label>
                            </div>
                        </div>
                        <?php
                        $data_for_field = array();
                        $custom_post_name = 'destination';
                        $args_custom = array( 'post_type' => $custom_post_name ,'post_status' => 'publish', 'posts_per_page' => -1);
                        $query_custom = null;
                        $query_custom = new WP_Query($args_custom);
                        if ($query_custom->have_posts()) {
                            while ($query_custom->have_posts()) : $query_custom->the_post();
                                $theID = get_the_ID();
                                array_push($data_for_field, get_the_title());

                            endwhile;
                            wp_reset_postdata();
                        }
                        ?>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Destinations</label>
                            <div class="col-sm-9">

                                <?php

                                echo '<select class="multiselectDestinations" name="destination_we_support" multiple="multiple">';
                                foreach( $data_for_field as $option_single ){

                                    $name_option= $option_single;
                                    echo "<option value='$name_option'> $name_option </option>";

                                }
                                echo "<select>";

                                ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-9">
                                <label>Please enter your ideal destination(s) and hit Enter</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="wishList" class="col-sm-3 control-label">Your wish list</label>
                            <div class="col-sm-9">
                                <div id="wishList" class="tag-list"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="guests" class="col-sm-3 control-label">Comments</label>
                            <div class="col-sm-9">
                                <textarea name="comments" class="form-control" id="" placeholder="Comments regarding your tour"></textarea>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default pull-right">Send</button>
                            </div>
                        </div>
                    </form>

                    <div class="error-template" style="text-align: center ; display: none">
                        <h1>
                            Thanks!</h1>
                        <div class="error-details">
                        </div>
                        <br/>
                        <div class="error-actions">
                            <a href="<?= get_site_url(); ?>" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-home"></span>
                                Take Me Home </a>
                            <a href="<?= get_site_url(); ?>/contact-us" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-envelope"></span> Contact Support </a>
                        </div>
                    </div>

                </div>


            </div>
        </div>
    </section>

    <script>
        $(document).ready(function () {
            $("#customizeTour").validate({
                submitHandler: function(form) {

                    $('.error-details').html('please wait..');
                    $('#customizeTour').hide(function () {
                        $('.error-template').show();

                    });


                    var data = $( form ).serializeArray();
                    data.push({
                        name : 'wishList',
                        value : jQuery('#wishList').tags().getTags()
                    });
                    console.log(data);

                    $.ajax({
                        method: "POST",
                        url: "<?= get_permalink() ?>",
                        data: {
                            'customize_tour' : data
                        }
                    })
                        .done(function( msg ) {
                            $('.error-details').html('Thank you. We will contact you within 1-2 business days.');
                            $('#customizeTour').hide(function () {
                                $('.error-template').show();

                            });
                            console.log( msg );

                        });

                }
            });
        })
    </script>

<?php get_footer(); ?>