<?php
/**
 * Created by PhpStorm.
 * User: thilinaperera
 * Date: 11/28/14
 * Time: 10:11 PM
 */
get_header();
?>

<?php
// Start the Loop.
while ( have_posts() ) : the_post()?>

   <?php


    $theID = get_the_ID();
    $theTitle = get_the_title($theID);
    $theSubTitle = get_post_meta( $theID, 'details_sub_heading', true );
    $theFullThumb = get_the_post_thumbnail( $theID, 'full' );

    $theFullThumb_URL =  wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ) );


    $theToGallery = array();
    $names = ['top_photo_gallery_first_image','top_photo_gallery_second_image','top_photo_gallery_third_image','top_photo_gallery_fourth_image','top_photo_gallery_fifth_image', 'top_photo_gallery_sixth_image'];

    foreach($names as $name){
        $content = json_decode(get_post_meta( $theID, $name, true ),true);
        if($content != null){
            array_push($theToGallery,$content);
        }

    }


    ?>


    <div class="sectionTopImage">
        <div class="container">
            <div class="row topImageRow" style="background: url('<?= $theFullThumb_URL ?>') center">


                <div class="imageText">
                    <div class="text container">
                        <div class="row textDetails">
                            <h3><?= $theTitle  ?></h3>
                            <span class="subText"><?= $theSubTitle  ?></span>
                        </div>
                    </div>
                </div>
                <div class="flipper"></div>
            </div>
        </div>
    </div>


    <section class="postSection">
        <div class="container postWithSidebar">
            <div class="row postRow">

                <?php get_sidebar();  ?>





                <?php

                $post_type = get_post_type( $theID );
                // condition ? if condition true run this : or (false) then run this
                $post_type == 'package' ? include 'include/templates/package-post.tmpl.php' : include 'include/templates/single-post.tmpl.php';



                ?>

            </div>
        </div>
    </section>


<?php endwhile;
wp_reset_postdata();
?>

<?php get_footer(); ?>