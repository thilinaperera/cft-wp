<?php

/**
 * Calls the class on the post edit screen.
 */

if ( is_admin() ) {
    add_action( 'load-post.php', 'call_someClass' );
    add_action( 'load-post-new.php', 'call_someClass' );
}

/**
 * The Class.
 */
class Custom_Fields {

    /**
     * Hook into the appropriate actions when the class is constructed.
     */

    private $post_type;
    private $fields;

    public function __construct($post_type =array(), $fields = array()) {


        $this->post_type = $post_type;
        $this->fields = $fields;

        add_action( 'add_meta_boxes', array( $this, 'add_meta_box') );
        add_action( 'save_post', array( $this, 'save' ) );
    }

    /**
     * Adds the meta box container.
     */
    public function add_meta_box( $post_type ) {


        $post_types = $this->post_type;     //limit meta box to certain post types
        if ( in_array( $post_type, $post_types )) {
            add_meta_box(
                'Page Details'
                ,__( 'Page Details', 'myplugin_textdomain' )
                ,array( $this, 'render_meta_box_content' )
                ,$post_type
                ,'advanced'
                ,'high'
            );
        }
    }

    /**
     * Save the meta when the post is saved.
     *
     * @param int $post_id The ID of the post being saved.
     */
    public function save( $post_id ) {

        /*
         * We need to verify this came from the our screen and with proper authorization,
         * because save_post can be triggered at other times.
         */

        // Check if our nonce is set.
        if ( ! isset( $_POST['myplugin_inner_custom_box_nonce'] ) )
            return $post_id;

        $nonce = $_POST['myplugin_inner_custom_box_nonce'];

        // Verify that the nonce is valid.
        if ( ! wp_verify_nonce( $nonce, 'myplugin_inner_custom_box' ) )
            return $post_id;

        // If this is an autosave, our form has not been submitted,
        //     so we don't want to do anything.
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
            return $post_id;

        // Check the user's permissions.
        if ( 'page' == $_POST['post_type'] ) {

            if ( ! current_user_can( 'edit_page', $post_id ) )
                return $post_id;

        } else {

            if ( ! current_user_can( 'edit_post', $post_id ) )
                return $post_id;
        }

        /* OK, its safe for us to save the data now. */

//        // Sanitize the user input.
//        $mydata = sanitize_text_field( $_POST['myplugin_new_field'] );
//
//        // Update the meta field.
//        update_post_meta( $post_id, '_my_meta_value_key', $mydata );

        foreach ($this->fields as $form_name => $form_type) {


            $form_name  = strtolower(str_replace(' ', '_', $form_name));


            // handle the files

            if (!empty($_FILES[$form_name]) ) {

                if ($_FILES[$form_name]["type"] == "image/png" || $_FILES[$form_name]["type"] == "image/jpeg" || $_FILES[$form_name]["type"] == "image/gif" || $_FILES[$form_name]["type"] == "image/jpg" || $_FILES[$form_name]["type"] == "image/JPEG") {
                    if ( ! function_exists( 'wp_handle_upload' ) ) require_once( ABSPATH . 'wp-admin/includes/file.php' );


                    $checker = array(
                        'original' => false,
                        'thumbnail' => false,
                        'large' => false
                    );
                    $original_url = '';
                    $large_url = '';
                    $url = '';

                    if($form_name == 'slider_slider_image'){
                        $goal_image_file_original = wp_upload_bits($_FILES[$form_name]['name'], null, file_get_contents($_FILES[$form_name]["tmp_name"]));
                        if(false == $goal_image_file_original['error']){

                            $image_ori = wp_get_image_editor($goal_image_file_original['file']); // Return an implementation that extends <tt>WP_Image_Editor</tt>
                            $image_ori->resize(1200, 600, true);
                            $image_ori->save($goal_image_file_original['file']);
                            $original_url = $goal_image_file_original['url'];
                            $checker['original'] = true;

                        }

                    }else{

                        $goal_image_file_original = wp_upload_bits($_FILES[$form_name]['name'], null, file_get_contents($_FILES[$form_name]["tmp_name"]));
                        if(false == $goal_image_file_original['error']){

                            //$image_ori = wp_get_image_editor($goal_image_file_original['file']); // Return an implementation that extends <tt>WP_Image_Editor</tt>
                            //$image_ori->save($goal_image_file_original['file']);
                            $original_url = $goal_image_file_original['url'];
                            $checker['original'] = true;
                            //print_r($original_url);

                        }

                    }





                    $goal_image_file_large= wp_upload_bits($_FILES[$form_name]['name'], null, file_get_contents($_FILES[$form_name]["tmp_name"]));
                    if(false == $goal_image_file_large['error']){

                        $image = wp_get_image_editor($goal_image_file_large['file']); // Return an implementation that extends <tt>WP_Image_Editor</tt>
                        $image->resize(500, 500, true);
                        $image->save($goal_image_file_large['file']);
                        $large_url = $goal_image_file_large['url'];
                        $checker['large'] = true;


                    }


                    $goal_image_file = wp_upload_bits($_FILES[$form_name]['name'], null, file_get_contents($_FILES[$form_name]["tmp_name"]));
                    if (false == $goal_image_file['error']) {


                        $image = wp_get_image_editor($goal_image_file['file']); // Return an implementation that extends <tt>WP_Image_Editor</tt>
                        $image->resize(380, 200, true);
                        $image->save($goal_image_file['file']);
                        $url = $goal_image_file['url'];
                        $checker['thumbnail'] = true;

                        // Since we've already added the key for this, we'll just update it with the file.

                    }



                    if($checker['original'] && $checker['thumbnail']){


                        $data = json_encode(array(
                            'thumbnail' => $url,
                            'original' => $original_url,
                            'large' => $large_url
                        ));

                        update_post_meta($post_id, $form_name,$data );

                    }
                }

            }
            else{

                // value = '' then update it
                if (!isset($_POST[$form_name])) $_POST[$form_name] = '';
                if (isset($post_id) ) {
                    update_post_meta($post_id, $form_name, $_POST[$form_name]);
                }
            }
        }



    }


    /**
     * Render Meta Box content.
     *
     * @param WP_Post $post The post object.
     */
    public function render_meta_box_content( $post ) {


        $inputs = $this->fields;
        $meta = get_post_custom($post->ID);

        wp_nonce_field( 'myplugin_inner_custom_box', 'myplugin_inner_custom_box_nonce' );

        foreach ($inputs as $name => $type) {
            $id_name = strtolower(str_replace(' ', '_', $name));
            if (is_array($inputs[$name])) {
                if (strtolower($inputs[$name][0]) === 'select') {
                    $select = "<select name='$id_name' class='widefat'>";
                    foreach ($inputs[$name][1] as $option) {
                        // if what's stored in the db is equal to the
                        // current value in the foreach, that should
                        // be the selected one
                        if (isset($meta[$id_name]) && $meta[$id_name][0] == $option) {
                            $set_selected = "selected='selected'";
                        } else $set_selected = '';
                        $select .= "<option value='$option' $set_selected> $option </option>";
                    }
                    $select .= "</select>";

                }
            }


            $value = isset($meta[$id_name][0]) ? $meta[$id_name][0] : '';
            $checked = ($type == 'checkbox' && !empty($value) ? 'checked' : '');


            $lookup = array(
                "text" => "<input type='text' name='$id_name' value='$value' class='widefat' />",
                "textarea" => "<textarea name='$id_name' class='widefat' rows='10'>$value</textarea>",
                "checkbox" => "<input type='checkbox' name='$id_name' value='$name' $checked />",
                "select" => isset($select) ? $select : '',
                "file" => "<input type='file' name='$id_name' id='$id_name' />"
            );
            ?>

            <p>
                <label><?php echo ucwords($name) . ':'; ?></label>
                <?php echo $lookup[is_array($type) ? $type[0] : $type]; ?>
            </p>
            <?php


            if( is_array($type) ){
                $field_type = $type['type'];

                $data_for_field = '';
                // get data from data source
                if($type['source'] == 'post'){
                    // get data for post
                    $data_for_field = array();
                    $custom_post_name = $type['source_name'];
                    $args_custom = array( 'post_type' => $custom_post_name ,'post_status' => 'publish', 'posts_per_page' => -1);
                    $query_custom = null;
                    $query_custom = new WP_Query($args_custom);
                    if ($query_custom->have_posts()) {
                        while ($query_custom->have_posts()) : $query_custom->the_post();
                            $theID = get_the_ID();
                            //echo get_post_meta( $theID, $type['post_field'] , true );
                            array_push($data_for_field, get_post_meta( $theID, $type['post_field'] , true ));
                        endwhile;
                        wp_reset_postdata();
                    }
                }


                $current_value = $value;

                if($field_type == 'select'){
                    $select_box = "<select name='$id_name'>";
                    foreach($data_for_field as $option_value){
                        if($option_value == $current_value){
                            $selected_opt = "selected='selected'";
                        }
                        else{
                            $selected_opt = '';
                        }
                        $select_box .= "<option value='$option_value' $selected_opt > $option_value </option>";
                    }
                    $select_box .= "<select>";
                    echo $select_box;
                }
            }

            ?>

            <p>

                <?php
                // If a file was uploaded, display it below the input.

                if($type == 'file'){
                    // display the image
                    $file = get_post_meta($post->ID, $id_name, true);

                    $file_array = json_decode($file,true);

                    if ( isset($file_array) ) {
                        $thumbnail= $file_array['thumbnail'];
                        $original= $file_array['original'];
                        echo "<img src='$thumbnail' alt='' style='max-width: 200px;' />";
                        echo "<br>";
                        echo "<a href='$original'>Original file link</a>";
                        echo "<hr/>";
                        echo "<br>";
                    }
                }
                ?>
            </p>
        <?php


        }







//        // Add an nonce field so we can check for it later.
//        wp_nonce_field( 'myplugin_inner_custom_box', 'myplugin_inner_custom_box_nonce' );
//
//        // Use get_post_meta to retrieve an existing value from the database.
//        $value = get_post_meta( $post->ID, '_my_meta_value_key', true );
//
//        // Display the form, using the current value.
//        echo '<label for="myplugin_new_field">';
//        _e( 'Description for this field', 'myplugin_textdomain' );
//        echo '</label> ';
//        echo '<input type="text" id="myplugin_new_field" name="myplugin_new_field"';
//        echo ' value="' . esc_attr( $value ) . '" size="25" />';
    }
}