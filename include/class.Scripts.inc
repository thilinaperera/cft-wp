<?php

class Scripts {
    public $dir;
    public function __construct($scripts = array()){

        $this->dir = JAVASCRIPTS;

        if(isset($scripts)){

            foreach($scripts as $name => $file){
                $name = strtolower(str_replace(' ', '_', $name));
                $filename = $file.'.js';
                $this->init($name,$filename);
                $this->enqueue($name);
            }
        }

    }
    private function init($name,$filename){
        wp_register_script($name, $this->dir . '/'.$filename, array('jquery'),1);
    }
    private function enqueue($name){
        wp_enqueue_script($name);
    }
}
