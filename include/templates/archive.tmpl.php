<?php
/**
 * Created by PhpStorm.
 * User: thilinaperera
 * Date: 12/1/14
 * Time: 12:02 AM
 */


?>
<section class="postSection">
    <div class="container postWithSidebar">
        <div class="row">
            <div class="col-lg-8 col-md-8 postText">


                <div class="row postsHolder">
                    <div class="posts col-lg-12 col-xs-12">

                        <?php

                        if(have_posts()) : while(have_posts()) : the_post();
                            $theID = get_the_ID();
                            //$theTitle = get_the_title($theID);
                            //$theSubTitle = get_post_meta( $theID, 'details_sub_heading', true );
                            $theLargeThumb = get_the_post_thumbnail( $theID, 'Large' );
                        ?>


                            <div class="post">
                                <h3><a href="<?php the_permalink();  ?>"><?php the_title(); ?></a></h3>
                                <div class="image">
                                    <?= $theLargeThumb ?>
                                </div>
                                <div class="text">
                                    <?php
                                    $content = get_the_content();
                                    $trimmed_content = wp_trim_words( $content, 100, '...' );
                                    ?>
                                    <?= $trimmed_content ?>
                                </div>
                                <ul class="post-share-social-media  fa-icon-effect-1 fa-icon-effect-1a clearfix">
                                    <li>
                                        <a href="http://www.facebook.com/share.php?u=<?= the_permalink(); ?>&amp;title=<?=  the_title() ?>" class="fa-icon fa fa-facebook" target="_blank">&nbsp;</a>
                                    </li>
                                    <li>
                                        <a href="http://twitter.com/home?status=<?= the_permalink(); ?>" class="fa-icon fa fa-twitter" target="_blank">&nbsp;</a>
                                    </li>
                                    <li>
                                        <a href="https://plus.google.com/share?url=<?= the_permalink(); ?>" target="_blank" class="fa-icon fa fa-google-plus">&nbsp;</a>
                                    </li>
                                    <li>
                                        <a href="http://pinterest.com/pin/create/bookmarklet/?url=<?= the_permalink(); ?>&amp;description=<?=  the_title() ?>" class="fa-icon fa fa-pinterest" target="_blank">&nbsp;</a>
                                    </li>
                                    <li>
                                        <a href="mailto:?subject=<?=  the_title() ?>&amp;body=Lookatthis...<?= the_permalink(); ?>" class="fa-icon fa fa-envelope-o" target="_blank">&nbsp;</a>
                                    </li>
                                </ul>
                            </div>


                        <?php

                        endwhile;
                            ?>

                            <nav>
                                <ul class="pager">
                                    <li class="previous">
                                        <?php previous_posts_link( 'Newer <span aria-hidden="true">&rarr;</span>' ); ?>

                                    </li>
                                    <li class="next">
                                        <?php next_posts_link( '<span aria-hidden="true">&larr;</span> Older' ); ?>
                                    </li>
                                </ul>
                            </nav>

                            <?php

                        endif;
                        ?>
                    </div>
                </div>


            </div>
            <?php get_sidebar(); ?>
        </div>
    </div>
</section>