<div class="col-lg-8 col-md-8 postText">


    <?php  if(count($theToGallery)){  ?>
        <div class="featured-image pfgallery">
            <ul class="post-thumbnail row">


                <?php
                foreach($theToGallery as $item ){
                    if(isset($item)){
                        ?>


                        <li class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                            <a data-rel="prettyPhoto[portfolio]" href="<?= $item['original'] ?>">
                                <img width="" height="" src="<?= $item['thumbnail'] ?>" class="attachment-medium" alt="">
                            </a>
                        </li>


                    <?php
                    }
                }
                ?>
            </ul>
        </div>
    <?php } ?>
    <?php the_content('Read more' . get_the_title()); ?>

</div>