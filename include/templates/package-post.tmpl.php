<div class="col-lg-8 col-md-8 packageText">

    <div role="tabpanel" class="packageDetailsHolder">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#overview" aria-controls="overview" role="tab" data-toggle="tab">Overview</a></li>
            <li role="presentation"><a href="#destinations" aria-controls="profile" role="tab" data-toggle="tab">Destinations</a></li>
            <li role="presentation"><a href="#timeline" aria-controls="timeline" role="tab" data-toggle="tab">Timeline</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade" id="timeline">
                <?php

                $total_days = get_post_meta( $post->ID, 'package_details_total_days' , true );

                ?>
                <ul class="cbp_tmtimeline">

                    <?php

                        $timeline_events = get_post_meta( $post->ID, 'timeline_events' , true );
                        $timeline_events = json_decode($timeline_events);
                        //echo $timeline_events;
                        //var_dump($timeline_events);
                        foreach($timeline_events as $value){
                            $day = $value->day;
                            $time = $value->time;
                            $title = $value->title;
                            $description = $value->description;

                            $item = '<li>
                                        <time class="cbp_tmtime"><span>Day '.$day.'</span> <span>'.$time.'</span></time>
                                        <div class="cbp_tmicon fa fa-calendar"></div>
                                        <div class="cbp_tmlabel">
                                            <h2>'.$title.'</h2>
                                            <p>'.$description.'</p>
                                        </div>
                                    </li>';

                            echo $item;
                        };
                    ?>


                </ul>


            </div>
            <div role="tabpanel" class="tab-pane fade in active" id="overview">



                <?= $post->post_content;  ?>


            </div>
            <div role="tabpanel" class="tab-pane fade" id="destinations">


                <div class="row allPostHolderRow">
                    <?php


                    // destinations for this package
                    $destinations = get_post_meta( $post->ID, 'package_details_destinations' , true );
                    $destinations = json_decode($destinations);


                    $args_section_des = array( 'post_type' => 'destination','post_status' => 'publish' ,'posts_per_page'=> -1 );
                    $query_section_des= null;
                    $query_section_des = new WP_Query($args_section_des);
                    if( $query_section_des->have_posts() ) : while( $query_section_des->have_posts() ) : $query_section_des->the_post();
                        //if ($query_section_des->have_posts()) {
                        //while ($query_section_des->have_posts()) : $query_section_des->the_post();

                        $theID = get_the_ID();
                        $theTitle = get_the_title($theID);
                        $theSubTitle = get_post_meta($theID, 'details_sub_heading', true);

                        $destination_type = get_post_meta($theID, 'details_destination_type', true);

                        $theSubTitle = wp_trim_words($theSubTitle, 10, ' ...');
                        $theContent = get_the_content($theID);
                        $theContent = wp_trim_words($theContent, 20, ' ...');
                        $thePermalink = get_the_permalink($theID);


                        $theToGallery = array();
                        $names = ['top_photo_gallery_first_image', 'top_photo_gallery_second_image', 'top_photo_gallery_third_image', 'top_photo_gallery_fourth_image', 'top_photo_gallery_fifth_image', 'top_photo_gallery_sixth_image'];

                        foreach ($names as $name) {
                            $content = json_decode(get_post_meta($theID, $name, true), true);

                            if ($content != null) {
                                array_push($theToGallery, $content);
                            }

                        }

                        $single = '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 singlePost">
                                                <a href="'.$thePermalink.'" class="post_link">
                                                    <img src="'.$theToGallery[0]['thumbnail'].'">
                                                    <figcaption class="_caption">
                                                        <span class="_catagory-name">'.$theTitle.'</span>
                                                    </figcaption>
                                                </a>
                                            </div>';
                        foreach($destinations as $destination){

                            if($destination == $theTitle){
                                echo $single;
                                break;
                            }

                        }





                    endwhile;

                    endif;






                    ?>

                </div>




            </div>
        </div>

    </div>

</div>