<?php


class Custom_Post {

    private $post_type_name;
    private $post_type_args;
    private $post_type_template;

    function __construct($post_type_name,$post_type_template,$post_type_args){

        $this->post_type_name = $post_type_name;
        $this->post_type_args = $post_type_args;
        $this->post_type_template = $post_type_template;

        // check uer options and merge with defaults
        $name = ucwords($this->post_type_name);
        $args_defaults = array(
            'labels' => array(
                'name' => $name,
                'singular_name' => $name,
                'add_new' => 'Add New '.$name,
                'add_new_item' => 'Add New '.$name,
                'edit' => 'Edit '.$name,
                'edit_item' => 'Edit '.$name,
                'new_item' => 'New '.$name,
                'view' => 'View '.$name,
                'view_item' => 'View '.$name,
                'search_items' => 'Search '.$name.'s',
                'not_found' => 'No '.$name.'s found',
                'not_found_in_trash' => 'No '.$name.'s found in Trash',
                'parent' => 'Parent '.$name.'s',
            ),
            'singular_label' => $name,
            'public' => true,
            'show_ui' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'rewrite' => true,
            'supports' => array('title','editor','thumbnail','comments'),
            'menu_position' => 105,
            'has_archive' => true,
            'show_in_menu'        => TRUE,
            'show_in_nav_menus' => true
        );
        // override the default options using user provided options
        $this->post_type_args  = array_merge($args_defaults,$this->post_type_args);
        // register new post
        $this->init();
        $this->save_post();
    }
    private function init(){
        try{
            register_post_type($this->post_type_name, $this->post_type_args);
        }
        catch(Exception $e){
            return 'Sorry enable to create post type';
        }
    }
    /**
     * Helper method, that attaches a passed function to the 'admin_init' WP action
     * @param function $cb Passed callback function.
     */
    function admin_init($cb)
    {
        add_action("admin_init", $cb);
    }



    /**
     * Registers a new taxonomy, associated with the instantiated post type.
     *
     * @param string $taxonomy_name The name of the desired taxonomy
     * @param string $plural The plural form of the taxonomy name. (Optional)
     * @param array $options A list of overrides
     */
    function add_taxonomy($taxonomy_name, $plural = '', $options = array())
    {
        // Create local reference so we can pass it to the init cb.
        $post_type_name = $this->post_type_name;
        // If no plural form of the taxonomy was provided, do a crappy fix. :)
        if (empty($plural)) {
            $plural = $taxonomy_name . 's';
        }
        // Taxonomies need to be lowercase, but displaying them will look better this way...
        $taxonomy_name = ucwords($taxonomy_name);
        // At WordPress' init, register the taxonomy
        $this->init(
            function() use($taxonomy_name, $plural, $post_type_name, $options)
            {
                // Override defaults with user provided options
                $options = array_merge(
                    array(
                        "label" => $taxonomy_name,
                        "singular_label" => $plural,
                        "show_ui" => true,
                        "query_var" => true,
                        'show_admin_column' => true,
                        'hierarchical'      => true,
                        "rewrite" => array("slug" => strtolower($taxonomy_name))
                    ),
                    $options
                );
                // name of taxonomy, associated post type, options
                register_taxonomy(strtolower($taxonomy_name), $post_type_name, $options);
            });
    }

    // add meta boxes the post
    function add_meta_box($title, $form_fields = array()){

        $post_type_name = $this->post_type_name;
        add_action('post_edit_form_tag', function()
        {
            echo ' enctype="multipart/form-data"';
        });
        global $post;
        global $custom_fields;

        $metabox = array(
            'post_type' => $post_type_name,
            'title' => strtolower(str_replace(' ', '_', $title)),
            'fields' => $form_fields
        );


        if(count($custom_fields) == 0 ){
            $custom_fields = array();
            array_push($custom_fields,$metabox);
        }

        array_push($custom_fields,$metabox);
        //$custom_fields[] = $form_fields;


        $this->admin_init(function() use($title,$form_fields,$post_type_name){

            add_meta_box(
                strtolower(str_replace(' ', '_', $title)), // id
                $title, // title
                function($post,$data){


                    // List of all the specified form fields
                    $inputs = $data['args'][0];
                    //$custom_fields = $inputs;

                    // Get the saved field values
                    $meta = get_post_custom($post->ID);


                    // For each form field specified, we need to create the necessary markup
                    // $name = Label, $type = the type of input to create
                    foreach ($inputs as $name => $type) {
                        $id_name = $data['id'] . '_' . strtolower(str_replace(' ', '_', $name));
                        if (is_array($inputs[$name])) {
                            if (strtolower($inputs[$name][0]) === 'select') {
                                $select = "<select name='$id_name' class='widefat'>";
                                foreach ($inputs[$name][1] as $option) {
                                    // if what's stored in the db is equal to the
                                    // current value in the foreach, that should
                                    // be the selected one
                                    if (isset($meta[$id_name]) && $meta[$id_name][0] == $option) {
                                        $set_selected = "selected='selected'";
                                    } else $set_selected = '';
                                    $select .= "<option value='$option' $set_selected> $option </option>";
                                }
                                $select .= "</select>";

                            }
                        }
                        $value = isset($meta[$id_name][0]) ? $meta[$id_name][0] : '';
                        $checked = ($type == 'checkbox' && !empty($value) ? 'checked' : '');


                        $lookup = array(
                            "text" => "<input type='text' name='$id_name' value='$value' class='widefat button' />",
                            "textarea" => "<textarea name='$id_name' class='widefat' rows='10'>$value</textarea>",
                            "checkbox" => "<input type='checkbox' name='$id_name' value='$name' $checked />",
                            "select" => isset($select) ? $select : '',
                            "file" => "<input type='file' name='$id_name' id='$id_name' />"
                        );
                        ?>

                        <p>
                            <label><?php echo ucwords($name) . ':'; ?></label>
                            <?php echo $lookup[is_array($type) ? $type[0] : $type]; ?>
                        </p>
                        <?php


                        if( is_array($type) ){
                            $field_type = $type['type'];

                            $data_for_field = '';
                            // get data from data source
                            if($type['source'] == 'post'){
                                // get data for post
                                $data_for_field = array();
                                $custom_post_name = $type['source_name'];
                                $args_custom = array( 'post_type' => $custom_post_name ,'post_status' => 'publish', 'posts_per_page' => -1);
                                $query_custom = null;
                                $query_custom = new WP_Query($args_custom);
                                if ($query_custom->have_posts()) {
                                    while ($query_custom->have_posts()) : $query_custom->the_post();
                                        $theID = get_the_ID();
                                        //echo get_post_meta( $theID, $type['post_field'] , true );
                                        if($type['post_field'] == 'title'){
                                            array_push($data_for_field, get_the_title());
                                        }
                                        else{
                                            array_push($data_for_field, get_post_meta( $theID, $type['post_field'] , true ));
                                        }

                                    endwhile;
                                    wp_reset_postdata();
                                }
                            }


                            $current_value = $value;

                            if($field_type == 'select' || $field_type == 'multiselect'){


                                $multiple = 'multiple="multiple"';

                                if( $field_type != 'multiselect' ){
                                    $multiple = '';
                                }
                                else{
                                    $id_name = $id_name.'[]';
                                }


                               // echo $current_value;

                                foreach($data_for_field as $key => $option_value){

                                    // check for the multiselect
                                    if($field_type == 'multiselect' ){
                                        $saved_values = json_decode($current_value);
                                        if(is_array($saved_values)){
                                            foreach( $saved_values as $saved_value ){

                                                if($option_value == $saved_value){
                                                    //array_push($seleted_values,$option_value);
                                                    $data_for_field[$key] = array(

                                                        'selected' => true,
                                                        'name' => $option_value
                                                    );
                                                    break;
                                                }
                                                else{
                                                    $data_for_field[$key] = array(
                                                        'selected' => false,
                                                        'name' => $option_value
                                                    );
                                                }
                                            }
                                        }
                                        else{
                                            //echo 'not';
                                            $data_for_field[$key] = array(
                                                'selected' => false,
                                                'name' => $option_value
                                            );
                                        }


                                    }
                                    else{
                                        if($option_value == $current_value){

                                            $data_for_field[$key] = array(
                                                'selected' => true,
                                                'name' => $option_value
                                            );
                                        }
                                        else{
                                            $data_for_field[$key] = array(
                                                'selected' => false,
                                                'name' => $option_value
                                            );
                                        }
                                    }




                                }


                                echo "<select class='multiselect' name='$id_name' $multiple >";
                                foreach( $data_for_field as $option_single ){



                                    $name_option= $option_single['name'];
                                    $is_selected = $option_single['selected'];
                                    if($is_selected){
                                        echo "<option value='$name_option' selected='selected' > $name_option </option>";
                                    }
                                    else{
                                        echo "<option value='$name_option'> $name_option </option>";
                                    }

                                }
                                echo "<select>";
                                echo '
                                    <script>
                                        jQuery(".multiselect").multiselect();
                                    </script>
                                ';
                                //if(is_array())

//                                echo "<select name='$id_name' $multiple >";
//
//
//                                echo "<option value='$option' $selected > $option </option>";
//
//                                echo "<select>";

                            }


                            if($field_type == 'app'){
                                $saved_data = get_post_meta($post->ID, $id_name, true);

                                $app = $type['app_name'];
                                $controller = $type['controller'];
                                if(!$saved_data){
                                    $saved_data = '[]';
                                }
                                ?>
                                <script>
                                    var data_saved = <?=  $saved_data    ?>
                                </script>
                                <div class="container" style="width: 100%;padding: 0px">
                                    <div ng-app="<?= $app ?>" ng-controller="<?= $controller ?>" >

                                        <?= file_get_contents(TEMPLATES."/app.html"); ?>
                                        <input name="<?= $id_name ?>"  type="hidden" value="{{ data || json }}" />

                                    </div>
                                </div>
                            <?php

                            }

                        }

                        ?>

                        <p>

                            <?php
                            // If a file was uploaded, display it below the input.

                            if($type == 'file'){
                              // display the image
                                $file = get_post_meta($post->ID, $id_name, true);

                                $file_array = json_decode($file,true);

                                if ( isset($file_array) ) {
                                    $thumbnail= $file_array['thumbnail'];
                                    $original= $file_array['original'];
                                    echo "<img src='$thumbnail' alt='' style='max-width: 200px;' />";
                                    echo "<br>";
                                    echo "<a href='$original'>Original file link</a>";
                                    echo "<hr/>";
                                    echo "<br>";
                                }
                            }
                            ?>
                        </p>
                    <?php


                    }

                },
                $post_type_name, // associated post type
                'normal', // location/context. normal, side, etc.
                'default', // priority level
                array($form_fields) // optional passed arguments. this things can accessed using $data in init function
            );
            //global $custom_fields;
            //echo json_encode($custom_fields);
        });



    }


    // post save
    private function save_post(){
        //global $custom_fields;
        //echo json_encode($custom_fields);
        $post_type_name = $this->post_type_name;

        add_action('save_post', function() use($post_type_name){
            // remove auto save facility return false
            if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;

            // save post for get post is
            global $post;

            if( isset( $_POST ) && isset( $post->ID ) && get_post_type( $post->ID ) == $post_type_name ){
                global $custom_fields;
                //echo json_encode($custom_fields);
                //echo '<br/>';
                //echo $post_type_name;

                // get all fields relevant to the post type
                $fields_for_this_post_unarrange = array();
                $fields_for_this_post = array();
                foreach($custom_fields as $key => $value){
                    if($value['post_type'] == $post_type_name){
                        foreach($value['fields'] as $k => $v){
                            array_push($fields_for_this_post,$value['title'].'_'.strtolower(str_replace(' ', '_', $k)));
                        }
                        array_push($fields_for_this_post_unarrange,$value['fields']);
                    }
                }


                // saving part

                foreach ($fields_for_this_post as $form_name) {


                    // handle the files

                    if (!empty($_FILES[$form_name]) ) {

                        if ($_FILES[$form_name]["type"] == "image/png" || $_FILES[$form_name]["type"] == "image/jpeg" || $_FILES[$form_name]["type"] == "image/gif" || $_FILES[$form_name]["type"] == "image/jpg" || $_FILES[$form_name]["type"] == "image/JPEG") {
                            if ( ! function_exists( 'wp_handle_upload' ) ) require_once( ABSPATH . 'wp-admin/includes/file.php' );


                            $checker = array(
                                'original' => false,
                                'thumbnail' => false,
                                'large' => false
                            );
                            $original_url = '';
                            $large_url = '';
                            $url = '';




                            if($form_name == 'slider_slider_image'){
                                $goal_image_file_original = wp_upload_bits($_FILES[$form_name]['name'], null, file_get_contents($_FILES[$form_name]["tmp_name"]));
                                if(false == $goal_image_file_original['error']){

                                    $image_ori = wp_get_image_editor($goal_image_file_original['file']); // Return an implementation that extends <tt>WP_Image_Editor</tt>
                                    $image_ori->resize(1200, 600, true);
                                    $image_ori->save($goal_image_file_original['file']);
                                    $original_url = $goal_image_file_original['url'];
                                    $checker['original'] = true;

                                }

                            }else{

                                $goal_image_file_original = wp_upload_bits($_FILES[$form_name]['name'], null, file_get_contents($_FILES[$form_name]["tmp_name"]));
                                if(false == $goal_image_file_original['error']){

                                    //$image_ori = wp_get_image_editor($goal_image_file_original['file']); // Return an implementation that extends <tt>WP_Image_Editor</tt>
                                    //$image_ori->save($goal_image_file_original['file']);
                                    $original_url = $goal_image_file_original['url'];
                                    $checker['original'] = true;
                                    //print_r($original_url);

                                }

                            }





                            $goal_image_file_large= wp_upload_bits($_FILES[$form_name]['name'], null, file_get_contents($_FILES[$form_name]["tmp_name"]));
                            if(false == $goal_image_file_large['error']){

                                $image = wp_get_image_editor($goal_image_file_large['file']); // Return an implementation that extends <tt>WP_Image_Editor</tt>
                                $image->resize(500, 500, true);
                                $image->save($goal_image_file_large['file']);
                                $large_url = $goal_image_file_large['url'];
                                $checker['large'] = true;


                            }


                            $goal_image_file = wp_upload_bits($_FILES[$form_name]['name'], null, file_get_contents($_FILES[$form_name]["tmp_name"]));
                            if (false == $goal_image_file['error']) {


                                $image = wp_get_image_editor($goal_image_file['file']); // Return an implementation that extends <tt>WP_Image_Editor</tt>
                                $image->resize(380, 200, true);
                                $image->save($goal_image_file['file']);
                                $url = $goal_image_file['url'];
                                $checker['thumbnail'] = true;

                                // Since we've already added the key for this, we'll just update it with the file.

                            }



                            if($checker['original'] && $checker['thumbnail']){


                                $data = json_encode(array(
                                    'thumbnail' => $url,
                                    'original' => $original_url,
                                    'large' => $large_url
                                ));

                                update_post_meta($post->ID, $form_name,$data );

                            }
                        }

                    }
                    else{

                        // value = '' then update it

                        if (!isset($_POST[$form_name])) $_POST[$form_name] = '';
                        if (isset($post->ID) ) {
                            $value = $_POST[$form_name];
                            if(is_array($_POST[$form_name])){
                                $value =  json_encode($_POST[$form_name]);
                            }
                            update_post_meta($post->ID, $form_name, $value);
                        }
                    }
                }


            }

            return;
        });
    }


}
