<?php

class Style {
    public $dir;
    public function __construct($styles = array()){

        $this->dir = STYLES;

        if(isset($styles)){

            foreach($styles as $name => $file){
                $name = strtolower(str_replace(' ', '_', $name));
                $filename = $file.'.css';
                $this->init($name,$filename);
                $this->enqueue($name);
            }
        }

    }
    private function init($name,$filename){
        wp_register_style($name, $this->dir . '/'.$filename);
    }
    private function enqueue($name){
        wp_enqueue_style($name);
    }
}
