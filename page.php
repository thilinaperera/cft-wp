<?php
get_header();
?>
    <div class="sectionTopImage">
        <div class="container">
            <?php

            $theFullThumb_URL =  wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ) );


            ?>
            <div class="row topImageRow" style="background: url('<?= $theFullThumb_URL ?>') center">

                <div class="imageText">
                    <div class="text container">
                        <div class="row textDetails">
                            <h3> <?= get_the_title(); ?> </h3>
                            <span class="subText"><?= get_post_meta(get_the_ID(), 'sub_heading', true);  ?></span>
                        </div>
                    </div>
                </div>
                <div class="flipper"></div>
            </div>
        </div>
    </div>

    <section class="postSection">
        <div class="container postWithSidebar">
            <div class="row postRow">

                <?php get_sidebar(); ?>

                <div class="col-lg-8 col-md-8 postText">


                    <?= $post->post_content;  ?>


                </div>

            </div>
        </div>
    </section>


<?php get_footer(); ?>