<?php
/**
 * cft functions file
 *
 * @package WordPress
 * @subpackage cft
 * @since cft 1.0
 */


/******************************************************************************\
 *
 * Global defines
 *
/*******************************************************************************/
define('THEMENAME', 'cft');
define('THEMEROOT', get_stylesheet_directory_uri());
define('IMAGES', THEMEROOT . '/images');
define('JAVASCRIPTS', THEMEROOT . '/scripts');
define('STYLES', THEMEROOT . '/styles');
define('TEMPLATES',THEMEROOT.'/include/templates');






/******************************************************************************\
Extra - Remove menu items
\******************************************************************************/
add_action('after_setup_theme', 'cft_setup');
function cft_setup() {
    add_action('admin_init', 'remove_menus');
}

function remove_menus(){
    remove_menu_page( 'index.php' );
    remove_menu_page( 'edit.php' );
    remove_menu_page( 'edit-comments.php' );
    if(!current_user_can('manage_options')) {
        remove_menu_page( 'edit.php?post_type=page' );
        //remove_menu_page( 'upload.php' );
    }
    if(!current_user_can('manage_network')) {
        //remove_menu_page( 'themes.php' );                 //Appearance
        remove_submenu_page( 'themes.php', 'themes.php');  // themes
        remove_submenu_page( 'themes.php', 'theme-editor.php'); // editor

        global $submenu;
        unset($submenu['themes.php'][6]); // remove customize link

        remove_menu_page( 'plugins.php' );                //Plugins
        remove_menu_page( 'tools.php' );                  //Tools
        remove_menu_page( 'options-general.php' );        //Settings
        //remove_menu_page( 'customize.php' );


    }
}








/******************************************************************************\
styles class
\******************************************************************************/
require "include/class.Styles.inc";



$styles = new Style(array(
        'main' => 'a9ba3dcb.main',
        'vendor' => '375d021c.vendor',
        'tags' => 'bootstrap-tags',
        'lightbox' => 'lightbox'
    ));


/******************************************************************************\
scripts class
\******************************************************************************/
require "include/class.Scripts.inc";

$scripts = new Scripts(array(
    'vendor' => '398172e9.vendor',
    'tags' => 'bootstrap-tags.min',
    'main' => 'e613f06e.main',
    'masonry' => 'masonry.pkgd.min',
    'image' => 'imagesloaded.pkgd.min',
    'extras' => 'scripts',
    //'nav' => 'sidebar_menu_script'
    'lightbox' => 'lightbox.min'
));


/******************************************************************************\
custom navigation class
\******************************************************************************/
require "include/class.Nav.inc";



if ( is_admin() ) {
    $angular = new Scripts(array(
        'angular' => 'angular.min',
    ));
}




/******************************************************************************\
custom fields class
\******************************************************************************/
require "include/class.Fields.inc";
$fields = new Custom_Fields(
    array('page'),
    array('Sub Heading' => 'text')
);



/******************************************************************************\
 custom post class
\******************************************************************************/
require "include/class.Post.inc";



// destinations
$destination_post = new Custom_Post('destination','',array(
    'labels' => array(
        'name' => 'Destination'
    ),
    'supports' => array('title','editor','thumbnail','comments'),
    'has_archive' => false,
    'taxonomies'  => array('destination')
));
$destination_post->add_meta_box('Details',array(
    'sub heading' => 'text',
    'destination type' => array(
        'type' => 'select',
        'source' => 'post',
        'source_name' => 'area',
        'post_field' => 'destination_areas_destination_area'
    )
));
$destination_post->add_meta_box('Top Photo Gallery',array(
    'First Image' => 'file',
    'Second Image' => 'file',
    'Third Image' => 'file',
    'Fourth Image' => 'file',
    'Fifth Image' => 'file',
    'Sixth Image' => 'file'
));
$destination_post->add_taxonomy('destination', 'Destinations', array(
    'show_ui' => true
));

// destination types
$destination_categories = new Custom_Post('area','',array(
    'labels' => array(
        'name' => 'Areas',
        'singular_name' => 'Area'
    ),
    'has_archive' => false,
    'supports' => array('title'),
));
$destination_categories->add_meta_box('Destination Areas',array(
    'Destination Area' => 'text'
));


// attractions

$attractions = new Custom_Post('attraction','',array(
    'labels' => array(
        'name' => 'Attractions',
        'singular_name' => 'Attraction'
    ),
    'has_archive' => false
));
$attractions->add_meta_box('Details',array(
    'sub heading' => 'text',
    'Attraction Category' => array(
        'type' => 'select',
        'source' => 'post',
        'source_name' => 'attraction',
        'post_field' => 'attraction_category_category'
    )
));
$attractions->add_meta_box('Top Photo Gallery',array(
    'First Image' => 'file',
    'Second Image' => 'file',
    'Third Image' => 'file',
    'Fourth Image' => 'file',
    'Fifth Image' => 'file',
    'Sixth Image' => 'file'
));


$attractions_category = new Custom_Post('attraction-tag','',array(
    'labels' => array(
        'name' => 'Attractions Categories',
        'singular_name' => 'Attraction Category'
    ),
    'supports' => array('title')
));
$attractions_category->add_meta_box('Attraction Category',array(
    'Category' => 'text'
));






// events
$events_post = new Custom_Post('event','',array(
    'taxonomies'  => array('event'),
    'has_archive' => false,
));
$events_post->add_meta_box('Details',array(
    'sub heading' => 'text'
));
$events_post->add_meta_box('Top Photo Gallery',array(
    'First Image' => 'file',
    'Second Image' => 'file',
    'Third Image' => 'file',
    'Fourth Image' => 'file',
    'Fifth Image' => 'file',
    'Sixth Image' => 'file'
));


// activities

$activities_post = new Custom_Post('activity','',array(
    'has_archive' => false
));
$activities_post->add_meta_box('Details',array(
    'sub heading' => 'text'
));
$activities_post->add_meta_box('Top Photo Gallery',array(
    'First Image' => 'file',
    'Second Image' => 'file',
    'Third Image' => 'file',
    'Fourth Image' => 'file',
    'Fifth Image' => 'file',
    'Sixth Image' => 'file'
));


// packages

$packages = new Custom_Post('package','',array(
    'has_archive' => false,
));
$packages->add_meta_box('Details',array(
    'sub heading' => 'text'
));
$packages->add_meta_box('Price Details',array(
    'price' => 'text',
    'Old price' => 'text',
    'To front page' => 'checkbox'
));
$packages->add_meta_box('Package Details',array(
    'destinations' => array(
        'type' => 'multiselect',
        'source' => 'post',
        'source_name' => 'destination',
        'post_field' => 'title'
    ),
    'description' => 'textarea',
    'total days' => 'text'
));

$packages->add_meta_box('Timeline',array(
    'Events' => array(
        'type' => 'app',
        'template' => 'app.html',
        'app_name' => 'App',
        'controller' => 'main'
    )
));


$packages->add_meta_box('Top Photo Gallery',array(
    'First Image' => 'file',
));





// $promoted

$featured = new Custom_Post('featured','',array(
    'has_archive' => false,
    'labels' => array(
        'name' => 'Featured Posts',
        'singular_name' => 'Featured'
    ),
    'show_in_menu'        => true,
    'show_in_nav_menus' => true
));
$featured->add_meta_box('Details',array(
    'sub heading' => 'text'
));
$featured->add_meta_box('Top Photo Gallery',array(
    'First Image' => 'file',
    'Second Image' => 'file',
    'Third Image' => 'file',
    'Fourth Image' => 'file',
    'Fifth Image' => 'file',
    'Sixth Image' => 'file'
));




// testimonial
$testimonials = new Custom_Post('testimonials','',array(
    'supports' => array('thumbnail','title'),
    'has_archive' => false,
    'labels' => array(
        'name' => 'Testimonials',
        'singular_name' => 'Testimonial'
    ),
    'show_in_menu'        => true,
    'show_in_nav_menus' => true,
    'public' => false,
    'rewrite' => false,
));
$testimonials->add_meta_box('Details',array(
    //'quote' => 'textarea',
    //'client' => 'text',
    //'address' => 'text',
    'image' => 'file',
));




// main slider
$slider = new Custom_Post('main_slider','',array(
    'labels' => array(
        'name' => 'Main Slider'
    ),
    'has_archive' => false,
    'supports' => array('title'),
    'show_in_menu'        => true,
    'show_in_nav_menus' => true
));
$slider->add_meta_box('Slider',array(
    'slider_image' => 'file',
));







/******************************************************************************\
	Theme support, standard settings, menus and widgets
\******************************************************************************/

add_theme_support( 'post-formats', array( 'image', 'quote', 'status', 'link' ) );
add_theme_support( 'post-thumbnails' );
add_theme_support( 'automatic-feed-links' );



/******************************************************************************\
Theme support, standard settings, menus and widgets
\******************************************************************************/

if ( function_exists('register_sidebar') ) {
    register_sidebar(array(
        'name' => 'Main Sidebar',
        'id' => 'main-sidebar',
        'description' => 'Appears as the sidebar on the pages',
    ));
}




function get_top_parent_page_id() {

    global $post;

    $ancestors = $post->ancestors;

    // Check if page is a child page (any level)
    if ($ancestors) {

        //  Grab the ID of top-level page from the tree
        return end($ancestors);

    } else {

        // Page is the top level, so use  it's own id
        return $post->ID;

    }

}


// add extra items to footer menu
// add new item to menu
add_filter('wp_nav_menu_items','social_links_in_menu', 10, 2);
function social_links_in_menu( $items, $args ) {
    if( $args->theme_location == 'footer' ) {
        $items .= '<li>
                        <a href="">Contact</a>

                        <div class="contact">

                            <div class="contact-item call">
                                <span class="icon"><i class="fa-icon fa fa-phone"></i> +94 77 08 543 76</span>
                            </div>
                            <div class="contact-item email">
                                <span class="icon"><i class="fa-icon fa fa-envelope"></i> sunimaldesilva@gmail.com</span>
                            </div>
                        </div>
                        <div class="social">
                            <a target="_blank" href="https://www.facebook.com/pages/Ceylon-Fascination-Tours/788007487957329?fref=ts" class="fa-icon facebook fa fa-facebook"></a>
                            <a target="_blank" href="https://twitter.com/ceyfascination" class="fa-icon twitter fa fa-twitter"></a>
                        </div>
                    </li>';
        return $items;
    }
    return $items;
}

