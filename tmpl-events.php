<?php
/*
Template Name: Events Template
*/
get_header();
?>
    <div class="sectionTopImage">
        <div class="container">
            <?php

            $theFullThumb_URL =  wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ) );


            ?>
            <div class="row topImageRow" style="background: url('<?= $theFullThumb_URL ?>') center">

                <div class="imageText">
                    <div class="text container">
                        <div class="row textDetails">
                            <h3> <?= get_the_title(); ?> </h3>
                            <span class="subText"><?= get_post_meta(get_the_ID(), 'sub_heading', true);  ?></span>
                        </div>
                    </div>
                </div>
                <div class="flipper"></div>
            </div>
        </div>
    </div>

    <section class="postSection">
        <div class="container postWithSidebar">
            <div class="row postRow">

                <?php get_sidebar(); ?>

                <div class="col-lg-8 col-md-8 postText">


                    <?= $post->post_content;  ?>
                    <div class="row allPostHolderRow masonryRow">
                    <?php


                        if ( get_query_var('paged') ) {
                            $paged = get_query_var('paged');
                        } else if ( get_query_var('page') ) {
                            $paged = get_query_var('page');
                        } else {
                            $paged = 1;
                        }
                        $args_section_des = array( 'post_type' => 'event','post_status' => 'publish' , 'max_num_pages' => 2, 'paged'  => $paged);
                        $query_section_des= null;
                        $query_section_des = new WP_Query($args_section_des);
                        if( $query_section_des->have_posts() ) : while( $query_section_des->have_posts() ) : $query_section_des->the_post();
                        //if ($query_section_des->have_posts()) {
                            //while ($query_section_des->have_posts()) : $query_section_des->the_post();

                                $theID = get_the_ID();
                                $theTitle = get_the_title($theID);
                                $theSubTitle = get_post_meta($theID, 'details_sub_heading', true);

                                $destination_type = get_post_meta($theID, 'details_destination_type', true);

                                $theSubTitle = wp_trim_words($theSubTitle, 10, ' ...');
                                $theContent = get_the_content($theID);
                                $theContent = wp_trim_words($theContent, 20, ' ...');
                                $thePermalink = get_the_permalink($theID);


                                $theToGallery = array();
                                $names = ['top_photo_gallery_first_image', 'top_photo_gallery_second_image', 'top_photo_gallery_third_image', 'top_photo_gallery_fourth_image', 'top_photo_gallery_fifth_image', 'top_photo_gallery_sixth_image'];

                                foreach ($names as $name) {
                                    $content = json_decode(get_post_meta($theID, $name, true), true);

                                    if ($content != null) {
                                        array_push($theToGallery, $content);
                                    }

                                }

                                $single = '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 singlePost masonryItem">
                                                <a href="'.$thePermalink.'" class="post_link">
                                                    <img src="'.$theToGallery[0]['thumbnail'].'">
                                                    <figcaption class="_caption">
                                                        <span class="_catagory-name">'.$theTitle.'</span>
                                                    </figcaption>
                                                </a>
                                            </div>';


                                echo $single;
                            endwhile;

                            ?>


                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 paginationHolder">
                                <nav class="paginationNav">
                                    <ul class="pagination">
                                        <li class="pull-left"><?= next_posts_link( '&#8592; Previous', $query_section_des->max_num_pages );  ?></li>
                                        <li class="pull-right"><?= previous_posts_link( 'Next &#8594;' );  ?></li>
                                    </ul>
                                </nav>
                            </div>

                            <?php





                        endif;

                        //wp_rest_postdata();




        ?>

                    </div>


                </div>

            </div>
        </div>
    </section>


<?php get_footer(); ?>