$(document).ready(function () {
    console.log('done');
    jQuery(".multiselectDestinations").multiselect();
    jQuery('#wishList').tags({
        tagSize: "lg",
        tagData: ["Destinations"]
    });
    var $container = jQuery('.masonryRow');
// initialize

    $container.imagesLoaded(function () {
        $container.masonry({
            //columnWidth: 200,
            itemSelector: '.masonryItem'
        });
    })

    var $container_testimonials = jQuery('.masonryRow');
    $container_testimonials.imagesLoaded(function () {
        $container_testimonials.masonry({
            //columnWidth: 200,
            itemSelector: '.masonryItem'
        });
    })

});