<?php
/**
 * cft Index template
 *
 * @package WordPress
 * @subpackage cft
 * @since cft 1.0
 */


get_header(); ?>

<?php the_content(); ?>

<?php get_footer(); ?>