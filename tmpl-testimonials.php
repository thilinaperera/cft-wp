<?php
/*
Template Name: Testimonials Template
*/
get_header();
?>
    <div class="sectionTopImage">
        <div class="container">
            <?php

            $theFullThumb_URL =  wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ) );


            ?>
            <div class="row topImageRow" style="background: url('<?= $theFullThumb_URL ?>') center">

                <div class="imageText">
                    <div class="text container">
                        <div class="row textDetails">
                            <h3> <?= get_the_title(); ?> </h3>
                            <span class="subText"><?= get_post_meta(get_the_ID(), 'sub_heading', true);  ?></span>
                        </div>
                    </div>
                </div>
                <div class="flipper"></div>
            </div>
        </div>
    </div>

    <section class="postSection">
        <div class="container postWithSidebar">
            <div class="row postRow">

                <?php get_sidebar(); ?>

                <div class="col-lg-8 col-md-8 postText testimonials_container">


                    <?= $post->post_content;  ?>

                    <div class="row testimonials masonryRow">
                    <?php



                    $args_section_des = array( 'post_type' => 'testimonials','post_status' => 'publish','posts_per_page'=> -1);
                    $query_section_des= null;
                    $query_section_des = new WP_Query($args_section_des);
                    if( $query_section_des->have_posts() ) : while( $query_section_des->have_posts() ) : $query_section_des->the_post();


                        $theID = get_the_ID();
                        $theTitle = get_the_title($theID);
                        $theSubTitle = get_post_meta($theID, 'details_sub_heading', true);

                        $theQuote = get_post_meta($theID,'details_quote',true);
                        $theAddress = get_post_meta($theID,'details_address',true);

                        $theQuote = wp_trim_words($theQuote, 20, ' ...');
                        $thePermalink = get_the_permalink($theID);
                        $theClient = get_post_meta($theID,'details_client',true);

                        $theClientImage_URLs =  get_post_meta($theID,'details_image',true);
                        $theClientImage_URLs = json_decode($theClientImage_URLs, true);

                        $single = '<div class="col-sm-6 person masonryItem">
                                                <a href="'.$theClientImage_URLs["original"].'" data-lightbox="roadtrip"><img src="'.$theClientImage_URLs["large"].'"  class="" data=""></a>
                                            </div>';

                        echo $single;

                    endwhile;
                    endif;


                    ?>

                    </div>


                </div>

            </div>
        </div>
    </section>


<?php get_footer(); ?>