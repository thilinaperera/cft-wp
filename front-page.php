<?php get_header(); ?>

    <div class="section mainSlider">
        <div class="customizerHolder container">
            <div class="customizer row">
                <div class="container customizerTextHolder">
                    <h3>Customize your tour</h3>
                    <span class="subtext">Just let us know the destination you would like to visit & we will take the hassles out from planning & executing such a fun trip.</span>

                    <form class="form-inline customizeForm" role="form" method="get" action="<?= get_site_url(); ?>/customize-your-tour/">

                        <div class="form-group">
                            <div class="input-group">
                                <label class="sr-only" for="">Check in</label>
                                <input name="checkin" type="text" class="form-control datepicker" placeholder="Check in">
                                <span class="glyphicon glyphicon-log-in form-control-feedback" aria-hidden="false"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <label class="sr-only" for="">Check out</label>
                                <input name="checkout" type="text" class="form-control datepicker" placeholder="Check out">
                                <span class="glyphicon glyphicon-log-out form-control-feedback" aria-hidden="false"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <label class="sr-only" for="">How many guests</label>
                                <input name="guests" type="text" class="form-control" id="" placeholder="How many guests">
                                <span class="glyphicon glyphicon-user form-control-feedback" aria-hidden="false"></span>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-default">Plan My Holidays</button>
                    </form>
                </div>


            </div>
        </div>
        <div class="row">
            <div class="flicker-slider" data-auto-flick-delay="6" data-dot-alignment="left" data-theme="light">
                <ul>

                    <?php
                    $args_slider = array( 'post_type' => 'main_slider', 'posts_per_page' => 6 ,'post_status' => 'publish');
                    $query_slider = null;
                    $query_slider = new WP_Query($args_slider);
                    $i = 0;
                    if ($query_slider->have_posts()) {
                        while ($query_slider->have_posts()) : $query_slider->the_post();
                            $theID = get_the_ID();
                            $theTitle = get_the_title($theID);

                            $images = json_decode(get_post_meta( $theID, 'slider_slider_image', true ),true);
                            ?>
                            <li data-block-text="false" data-background="<?= $images['original']  ?>"></li>
                        <?php

                        endwhile;
                        wp_reset_postdata();
                    }
                    ?>
                </ul>
            </div>
        </div>
    </div>

    <div class="section mainContentSection">
        <div class="container mainContent">
            <div class="row promoPackageHolder">


                <?php


                $args_section_promo = array( 'post_type' => 'package','post_status' => 'publish' ,'posts_per_page' => 3);
                $query_section_promo= null;
                $query_section_promo = new WP_Query($args_section_promo);
                $i = 0;
                if ($query_section_promo->have_posts()) {
                    while ($query_section_promo->have_posts()) : $query_section_promo->the_post();

                        $theID = get_the_ID();
                        $theTitle = get_the_title($theID);
                        $theSubTitle = get_post_meta($theID, 'details_sub_heading', true);

                        $promo_price = get_post_meta($theID, 'price_details_price', true);
                        $promo_price_old = get_post_meta($theID, 'price_details_old_price', true);
                        $promo_description = get_post_meta($theID, 'package_details_description', true);
                        $promo_description = wp_trim_words($promo_description, 30, ' ...');

                        $total_days = get_post_meta($theID, 'package_details_total_days' , true );

                        $theSubTitle = wp_trim_words($theSubTitle, 10, ' ...');
                        $theContent = get_the_content($theID);
                        $theContent = wp_trim_words($theContent, 20, ' ...');
                        $thePermalink = get_the_permalink($theID);


                        $theToGallery = array();
                        $names = ['top_photo_gallery_first_image', 'top_photo_gallery_second_image', 'top_photo_gallery_third_image', 'top_photo_gallery_fourth_image', 'top_photo_gallery_fifth_image', 'top_photo_gallery_sixth_image'];

                        foreach ($names as $name) {
                            $content = json_decode(get_post_meta($theID, $name, true), true);

                            if ($content != null) {
                                array_push($theToGallery, $content);
                            }

                        }


                        $promo = '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 promoPackage">
                                        <a href="'.$thePermalink.'" class="holder">
                                            <div class="imageHolder">
                                                <img class="img-responsive" src="'.$theToGallery[0]['thumbnail'].'">

                                                <div class="tag">
                                                    <div class="priceleftarchivetour">
                                                        <p>'.$promo_price.'</p>
                                                    </div>
                                                    <p class="titleleftarchivetour">
                                                        <span class="striketext">'.$promo_price_old.'</span>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="mainText">
                                                <h3>'.$theTitle.'</h3>

                                                <div class="dateTime">
                                                    <span class="date">'.$total_days.'</span>
                                                    <span>days</span>
                                                </div>
                                            </div>
                                            <p>
                                                '.$promo_description.'
                                            </p>
                                        </a>
                                    </div>';
                        echo $promo;
                    endwhile;
                }


                ?>

            </div>
        </div>
    </div>


    <section class="section destinationsSection">
        <div class="container destinationsContainer">
            <!--<div class="row">-->
            <!--<div class="col-lg-12 titleHolder">-->
            <!--<div class="gradientLine left hidden-xs"></div>-->
            <!--<h3>Destinations</h3>-->
            <!--<div class="gradientLine right hidden-xs"></div>-->
            <!--</div>-->
            <!--</div>-->
            <div class="row bannerRow">
                <div class="col-lg-12 bannerHolder">
                    <a href="<?= site_url() ?>/about" class="banner">
                        <h3>“Eco tourism, Personal touch & exceptional quality” is our mission, which will enable us to
                            achieve our vision, “The market leader in Eco tourism and livelihood development”</h3>

                        <p>Read our story</p>

                        <div class="flipper"></div>
                    </a>
                </div>
            </div>
            <div class="row destinationsRow">
                <div class="col-lg-12 destinationsHolder">
                    <section class="featured-destinations">


                        <?php

                        $args_section_des = array( 'post_type' => 'destination','post_status' => 'publish' ,'posts_per_page' => 5);
                        $query_section_des= null;
                        $query_section_des = new WP_Query($args_section_des);
                        $i = 0;
                        if ($query_section_des->have_posts()) {
                            while ($query_section_des->have_posts()) : $query_section_des->the_post();

                                $theID = get_the_ID();
                                $theTitle = get_the_title($theID);
                                $theSubTitle = get_post_meta($theID, 'details_sub_heading', true);

                                $destination_type = get_post_meta($theID, 'details_destination_type', true);

                                $theSubTitle = wp_trim_words($theSubTitle, 10, ' ...');
                                $theContent = get_the_content($theID);
                                $theContent = wp_trim_words($theContent, 20, ' ...');
                                $thePermalink = get_the_permalink($theID);


                                $theToGallery = array();
                                $names = ['top_photo_gallery_first_image','top_photo_gallery_second_image','top_photo_gallery_third_image','top_photo_gallery_fourth_image','top_photo_gallery_fifth_image', 'top_photo_gallery_sixth_image'];

                                foreach($names as $name){
                                    $content = json_decode(get_post_meta( $theID, $name, true ),true);

                                    if($content != null){
                                        array_push($theToGallery,$content);
                                    }

                                }


                                if($i == 0){
                                    $figure = '<figure class="_destination-block large">';
                                }
                                else{
                                    $figure = '<figure class="_destination-block small">';
                                }

                                $figure .= '
                                                <a href="'.$thePermalink.'" class="">
                                                    <img src="'.$theToGallery[0]['large'].'">

                                                    <figcaption class="_destination-caption">
                                                        <h2 class="_title">'.$theTitle.'</h2>

                                                        <div class="_description">
                                                            <p>'.$theSubTitle.'</p>
                                                            <span class="_link arrow-link">Learn More</span>
                                                        </div>
                                                    </figcaption>
                                                </a>
                                           ';

                                $figure .= ' </figure>';

                                echo $figure;



                                $i++;
                            endwhile;
                            wp_reset_postdata();
                        }


                        ?>

                    </section>


                </div>
            </div>
        </div>
    </section>




    <section class="section slidersSection">
        <div class="container">
            <div class="row slidersHolder">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <h3>Featured</h3>
                    <div id="carousel-events-generic" class="carousel slide" data-ride="">

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">

                            <?php
                            $args_featured = array( 'post_type' => 'featured', 'posts_per_page' => 3 ,'post_status' => 'publish');
                            $query_featured = null;
                            $query_featured = new WP_Query($args_featured);
                            $i = 0;
                            if ($query_featured->have_posts()) {
                                while ($query_featured->have_posts()) : $query_featured->the_post();

                                    $theID = get_the_ID();
                                    $theTitle = get_the_title($theID);
                                    $theTitle = wp_trim_words( $theTitle, 3, ' ...' );
                                    $theSubTitle = get_post_meta( $theID, 'details_sub_heading', true );
                                    $theSubTitle = wp_trim_words( $theSubTitle, 10, ' ...' );


                                    $theToGallery = array();
                                    $names = ['top_photo_gallery_first_image','top_photo_gallery_second_image','top_photo_gallery_third_image','top_photo_gallery_fourth_image','top_photo_gallery_fifth_image', 'top_photo_gallery_sixth_image'];

                                    foreach($names as $name){
                                        $content = json_decode(get_post_meta( $theID, $name, true ),true);

                                        if($content != null){
                                            array_push($theToGallery,$content);
                                        }

                                    }

                                ?>

                                    <a href="<?= the_permalink() ?>" class="item <?php if($i == 0){ echo 'active'; }  ?>">
                                        <img src="<?= $theToGallery[0]['thumbnail'] ?>" alt=""/>
                                        <div class="carousel-caption">
                                            <span class="title"><?= $theTitle ?></span>
                                            <span class="sub"><?= $theSubTitle  ?></span>
                                        </div>
                                    </a>

                                <?php
                                $i++;
                                endwhile;
                                wp_reset_postdata();
                            }
                            else{
                                echo 'Please upload images for the post ';
                            }
                            ?>

                        </div>

                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-events-generic" role="button" data-slide="prev">
                            <div class="glypHolder left">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                                <span class="sr-only">Previous</span>
                            </div>
                        </a>
                        <a class="right carousel-control" href="#carousel-events-generic" role="button" data-slide="next">
                            <div class="glypHolder right">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                                <span class="sr-only">Next</span>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <h3>Events</h3>
                    <div id="carousel-featured-generic" class="carousel slide" data-ride="">
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">

                            <?php
                            $args_featured = array( 'post_type' => 'event', 'posts_per_page' => 3 ,'post_status' => 'publish');
                            $query_featured = null;
                            $query_featured = new WP_Query($args_featured);
                            $i = 0;
                            if ($query_featured->have_posts()) {
                                while ($query_featured->have_posts()) : $query_featured->the_post();

                                    $theID = get_the_ID();
                                    $theTitle = get_the_title($theID);
                                    $theTitle = wp_trim_words( $theTitle, 3, ' ...' );
                                    $theSubTitle = get_post_meta( $theID, 'details_sub_heading', true );
                                    $theSubTitle = wp_trim_words( $theSubTitle, 10, ' ...' );

                                    $theToGallery = array();
                                    $names = ['top_photo_gallery_first_image','top_photo_gallery_second_image','top_photo_gallery_third_image','top_photo_gallery_fourth_image','top_photo_gallery_fifth_image', 'top_photo_gallery_sixth_image'];

                                    foreach($names as $name){
                                        $content = json_decode(get_post_meta( $theID, $name, true ),true);

                                        if($content != null){
                                            array_push($theToGallery,$content);
                                        }

                                    }

                                    ?>

                                    <a href="<?= the_permalink() ?>" class="item <?php if($i == 0){ echo 'active'; }  ?>">
                                        <img src="<?= $theToGallery[0]['thumbnail'] ?>" alt=""/>
                                        <div class="carousel-caption">
                                            <span class="title"><?= $theTitle ?></span>
                                            <span class="sub"><?= $theSubTitle  ?></span>
                                        </div>
                                    </a>

                                    <?php
                                    $i++;
                                endwhile;
                                wp_reset_postdata();
                            }
                            else{
                                echo 'Please upload images for the post ';
                            }
                            ?>

                        </div>

                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-featured-generic" role="button" data-slide="prev">
                            <div class="glypHolder left">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                                <span class="sr-only">Previous</span>
                            </div>
                        </a>
                        <a class="right carousel-control" href="#carousel-featured-generic" role="button" data-slide="next">
                            <div class="glypHolder right">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                                <span class="sr-only">Next</span>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <h3>Activities</h3>
                    <div id="carousel-activities-generic" class="carousel slide" data-ride="">
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">

                            <?php
                            $args_featured = array( 'post_type' => 'activity', 'posts_per_page' => 3 ,'post_status' => 'publish');
                            $query_featured = null;
                            $query_featured = new WP_Query($args_featured);
                            $i = 0;
                            if ($query_featured->have_posts()) {
                                while ($query_featured->have_posts()) : $query_featured->the_post();

                                    $theID = get_the_ID();
                                    $theTitle = get_the_title($theID);
                                    $theTitle = wp_trim_words( $theTitle, 3, ' ...' );
                                    $theSubTitle = get_post_meta( $theID, 'details_sub_heading', true );
                                    $theSubTitle = wp_trim_words( $theSubTitle, 10, ' ...' );

                                    $theToGallery = array();
                                    $names = ['top_photo_gallery_first_image','top_photo_gallery_second_image','top_photo_gallery_third_image','top_photo_gallery_fourth_image','top_photo_gallery_fifth_image', 'top_photo_gallery_sixth_image'];

                                    foreach($names as $name){
                                        $content = json_decode(get_post_meta( $theID, $name, true ),true);

                                        if($content != null){
                                            array_push($theToGallery,$content);
                                        }

                                    }

                                    ?>

                                    <a href="<?= the_permalink() ?>" class="item <?php if($i == 0){ echo 'active'; }  ?>">
                                        <img src="<?= $theToGallery[0]['thumbnail'] ?>" alt=""/>
                                        <div class="carousel-caption">
                                            <span class="title"><?= $theTitle ?></span>
                                            <span class="sub"><?= $theSubTitle  ?></span>
                                        </div>
                                    </a>

                                    <?php
                                    $i++;
                                endwhile;
                                wp_reset_postdata();
                            }
                            else{
                                echo 'Please upload images for the post ';
                            }
                            ?>

                        </div>

                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-activities-generic" role="button" data-slide="prev">
                            <div class="glypHolder left">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                                <span class="sr-only">Previous</span>
                            </div>
                        </a>
                        <a class="right carousel-control" href="#carousel-activities-generic" role="button" data-slide="next">
                            <div class="glypHolder right">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                                <span class="sr-only">Next</span>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>


<?php get_footer(); ?>