<div class="col-lg-4 col-md-4 sidebar">
    <ul class="sidebarList">

        <li class="widget widgetSidebar">
            <?php
            /*  Bootstrap menu  */
            $args = array(
                'menu' => 'sidebar',
                'theme_location' => 'sidebar',
                'depth'		 => 0,
                'container_id' => 'cssmenu',
                'walker'	 => new CSS_Menu_Maker_Walker()
            );
            wp_nav_menu($args);
            ?>
        </li>

        <li class="widget widgetSidebar">
            <h4 class="h4Heading">Search</h4>

            <form method="get" id="searchform" class="search-form" _lpchecked="1">
                <fieldset>
                    <input name="s" id="s" placeholder="Search this Site..." value="" type="text">
                    <input id="search-image" class="sbutton" src="<?= IMAGES ?>/search.png"
                           style="border: 0px none; vertical-align: top;" type="image">
                </fieldset>
            </form>
        </li>
        <li class="widget widgetSidebar">
            <h4 class="h4Heading">Featured</h4>


            <?php
            $args = array( 'post_type' => 'featured', 'posts_per_page' => 3 ,'post_status' => 'publish');
            $query = null;
            $query = new WP_Query($args);
            $i = 0;
            if ($query->have_posts()) {
                while ($query->have_posts()) : $query->the_post();

                    $theID = get_the_ID();
                    $theTitle = get_the_title($theID);
                    $theSubTitle = get_post_meta( $theID, 'details_sub_heading', true );
                    $theThumb = get_the_post_thumbnail( $theID,  array(80,80) );
                    $theMediumThumb = get_the_post_thumbnail( $theID, 'medium' );

                    if($i == 0){
                        // first post
                        ?>
                        <div class="featured big">
                            <a class="link" href="<?= the_permalink() ?>"><?= $theMediumThumb  ?></a>
                            <a class="link" href="<?= the_permalink() ?>"><?= $theTitle  ?></a>
                            <?php
                            $content = get_the_content();
                            $trimmed_content = wp_trim_words( $content, 40, '...' );
                            echo $trimmed_content;
                            ?>
                        </div>
                    <?php


                    }else{
                        ?>
                        <div class="featured">
                            <div class="row">
                                <div class="col-sm-4 column">
                                    <a href="<?= the_permalink() ?>">
                                        <?= $theThumb  ?>
                                    </a>
                                </div>
                                <div class="col-sm-8 column">
                                    <div class="recpostcats">
                                        <a href="<?= the_permalink() ?>" rel="category tag"><b><?= $theTitle  ?></b></a>
                                        <a href="<?= the_permalink() ?>" class="rpsmlink">
                                            <?php
                                            $content = get_the_content();
                                            $trimmed_content = wp_trim_words( $content, 20, '...' );
                                            echo $trimmed_content;
                                            ?>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                    }

                    ?>
                    <?php
                    $i++;
                endwhile;
                wp_reset_postdata();
            }
            ?>



        </li>
    </ul>
</div>


