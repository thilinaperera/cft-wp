<section class="section footer">


    <nav class="navbar-footer navbar navbar-default" role="navigation">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="">


                <?php
                /*  Bootstrap menu  */
                $args = array(
                    'menu' => 'footer',
                    'theme_location' => 'footer',
                    'depth'		 => 0,
                    'container'	 => false,
                    'menu_class'	 => 'nav navbar-nav',
                    'walker'	 => new BootstrapNavMenuWalker()
                );
                wp_nav_menu($args);
                ?>


            </div><!-- /.navbar-collapse -->
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>


    <div class="row information">
        <div class="container text-center">
            <p class="copyRights">Copyright © <?= date('Y'); ?> Ceylon Fascination Tours (Pvt) Ltd. All rights reserved.</p>
            <p class="text-muted-high" data-scroll-reveal="move 10px enter top over 1s no reset">Made with <i
                    class="icon-fast-food"></i> and <i class="icon-beer"></i> in Sri Lanka</p>

            <p class="text-muted-low" data-scroll-reveal="move 10px enter top over 1s no reset">Design & Developed By <a
                    href="http://thilinaperera.com" target="_blank">Thilina Perera</a> &copy; <?= date('Y'); ?></p>
        </div>
    </div>
</section>



<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
    ga('create','UA-XXXXX-X');ga('send','pageview');
</script>

<?php wp_footer(); ?>

</body>
</html>
